path[] make_bunny(transform t = identity()) {
pair pos = (0,0);
path[] result;
void add_path(path p) { result.push(t*p); }
// move
pos = (0,1.06392e-37);
// rel curve
add_path((0,1.06392e-37) .. controls (-1.11328,-0.28807) and (-2.21192,-0.56151) .. (-3.31055,-0.83984));
pos = (-3.31055,-0.83984);
add_path((-3.31055,-0.83984) .. controls (-5.21973,-0.75194) and (-7.12892,-0.66894) .. (-9.02833,-0.58594));
pos = (-9.02833,-0.58594);
add_path((-9.02833,-0.58594) .. controls (-11.5723,-1.04004) and (-10.9863,-0.9082) .. (-11.167,-3.0957));
pos = (-11.167,-3.0957);
add_path((-11.167,-3.0957) .. controls (-11.2695,-3.1641) and (-11.377,-3.22265) .. (-11.4746,-3.28125));
pos = (-11.4746,-3.28125);
add_path((-11.4746,-3.28125) .. controls (-11.5722,-5.67382) and (-9.79002,-6.25976) .. (-7.74901,-6.99706));
pos = (-7.74901,-6.99706);
add_path((-7.74901,-6.99706) .. controls (-7.57323,-7.50976) and (-7.57323,-7.50976) .. (-7.36815,-8.77441));
pos = (-7.36815,-8.77441);
add_path((-7.36815,-8.77441) .. controls (-9.44335,-9.375) and (-11.3916,-9.7168) .. (-12.7734,-11.4502));
pos = (-12.7734,-11.4502);
add_path((-12.7734,-11.4502) .. controls (-14.4238,-12.7295) and (-15.4004,-14.1943) .. (-16.084,-16.1523));
pos = (-16.084,-16.1523);
add_path((-16.084,-16.1523) .. controls (-17.2998,-18.2519) and (-16.7969,-20.0585) .. (-15.9522,-22.2509));
pos = (-15.9522,-22.2509);
add_path((-15.9522,-22.2509) .. controls (-15.9766,-22.5536) and (-16.0011,-22.8515) .. (-16.0108,-23.1347));
pos = (-16.0108,-23.1347);
add_path((-16.0108,-23.1347) .. controls (-19.0919,-24.0087) and (-19.9122,-25.6298) .. (-18.7598,-28.6035));
pos = (-18.7598,-28.6035);
add_path((-18.7598,-28.6035) .. controls (-18.3399,-30.3857) and (-17.9102,-32.1631) .. (-17.4805,-33.9404));
pos = (-17.4805,-33.9404);
add_path((-17.4805,-33.9404) .. controls (-16.2744,-35.6591) and (-13.4082,-35.8642) .. (-11.5479,-36.2109));
pos = (-11.5479,-36.2109);
add_path((-11.5479,-36.2109) .. controls (-11.2842,-36.8554) and (-11.0206,-37.4951) .. (-10.7471,-38.1445));
pos = (-10.7471,-38.1445);
add_path((-10.7471,-38.1445) .. controls (-9.19437,-39.6924) and (-6.96292,-38.872) .. (-6.38674,-36.8994));
pos = (-6.38674,-36.8994);
add_path((-6.38674,-36.8994) .. controls (-3.71584,-38.9209) and (-0.74222,-40.6006) .. (2.29001,-42.0215));
pos = (2.29001,-42.0215);
add_path((2.29001,-42.0215) .. controls (4.78513,-42.0215) and (5.00975,-38.9209) .. (4.95115,-37.0557));
pos = (4.95115,-37.0557);
add_path((4.95115,-37.0557) .. controls (4.74607,-36.8164) and (4.54587,-36.5772) .. (4.3408,-36.3379));
pos = (4.3408,-36.3379);
add_path((4.3408,-36.3379) .. controls (2.45603,-35.2197) and (-5.41018,-33.3106) .. (-5.41018,-31.0156));
pos = (-5.41018,-31.0156);
add_path((-5.41018,-31.0156) .. controls (-5.02933,-29.9511) and (-4.8633,-29.1406) .. (-3.97952,-28.4472));
pos = (-3.97952,-28.4472);
add_path((-3.97952,-28.4472) .. controls (-3.31058,-27.9247) and (-3.23245,-27.8808) .. (-0.12209,-27.3974));
pos = (-0.12209,-27.3974);
add_path((-0.12209,-27.3974) .. controls (0.88865,-27.2411) and (0.86423,-27.6416) .. (4.58006,-28.5449));
pos = (4.58006,-28.5449);
add_path((4.58006,-28.5449) .. controls (5.77635,-28.8379) and (7.01658,-29.0771) .. (8.2617,-29.0576));
pos = (8.2617,-29.0576);
add_path((8.2617,-29.0576) .. controls (13.0517,-28.9941) and (16.2109,-26.4453) .. (18.6181,-22.373));
pos = (18.6181,-22.373);
add_path((18.6181,-22.373) .. controls (19.038,-21.3574) and (19.4579,-20.3417) .. (19.8876,-19.331));
pos = (19.8876,-19.331);
add_path((19.8876,-19.331) .. controls (20.1464,-18.0517) and (20.1464,-18.0517) .. (20.5517,-13.4521));
pos = (20.5517,-13.4521);
add_path((20.5517,-13.4521) .. controls (22.1533,-13.335) and (23.1689,-13.3251) .. (24.0869,-11.9531));
pos = (24.0869,-11.9531);
add_path((24.0869,-11.9531) .. controls (25.1367,-7.749) and (22.6416,-5.63474) .. (19.1943,-3.88182));
pos = (19.1943,-3.88182);
add_path((19.1943,-3.88182) .. controls (17.3681,-3.22263) and (15.4785,-2.9736) .. (14.1064,-1.60154));
pos = (14.1064,-1.60154);
add_path((14.1064,-1.60154) .. controls (13.6767,-1.38669) and (13.2617,-1.17185) .. (12.8417,-0.957));
pos = (12.8417,-0.957);
add_path((12.8417,-0.957) .. controls (10.9325,-0.7031) and (10.9325,-0.7031) .. (-0.000100136,1.99676e-05));
pos = (-0.000100136,1.99676e-05);
return result;
}