unitsize(1cm);
pair b0 = (0,0);
pair b1 = (1,4);
pair b2 = (4,4);
pair b3 = (7,0);

path b = b0..b1..b2..b3;
fill(b0--b1--b2--b3--cycle, lightgrey);
draw(b0..controls b1 and b2 .. b3, red);
dot(b0);
label("$b_0$", b0, S);
dot(b1);
label("$b_1$", b1, N);
dot(b2);
label("$b_2$", b2, N);
dot(b3);
label("$b_3$", b3, S);
draw(b0--b1--b2--b3, dashed);
