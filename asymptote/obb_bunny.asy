import bunny_data;

path[] bunny = make_bunny();

picture obb;
draw(obb, bunny);

frame f = bbox(obb);

add(rotate(225) * f);
